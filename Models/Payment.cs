using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentApi.Models
{
    public class Payment
    {
        public EnumStatusPagamento Status { get; set; }
        public Vendedor Vendedor { get; set; }
        public DateTime Date { get; set; }
        public int Id { get; set; }
        public string Itens { get; set; }
    }
}