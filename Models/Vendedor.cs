using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentApi.Models
{
    public class Vendedor
    {
        public int Id { get; set; }
        public int cpf { get; set; }
        public string Nome { get; set; }
        public string email { get; set; }
        public int telefone { get; set; }
    }
}